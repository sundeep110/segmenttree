import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SegmentTree {
    float[] array;

    int sum =0;
    public void create() throws IOException {
        array = new float[6];
        System.out.printf("Please input a value:");
        for (int i = 0; i < 6; i++) {
            float userinput;
            InputStreamReader sr = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(sr);
            userinput = Float.parseFloat(br.readLine());
            array[i] = userinput;
        }
        Node root = CreateTree(0, array.length - 1);
        TraversePrint(root);
//        findUpdate(1, root, 3);
        //findUpdate2(1, root, 3);
        sum   = 0;
        sum = query(new IntervalRange(3,3),root);
        System.out.println("Sum " + sum);
        sum = 0;
        sum = query(new IntervalRange(0,3),root);
        System.out.println("Sum " + sum);

        sum   = 0;
        sum = query(new IntervalRange(2,3),root);
        System.out.println("Sum " + sum);

        sum   = 0;
        sum = query(new IntervalRange(0,2),root);
        System.out.println("Sum " + sum);

         sum   = 0;
        sum = query(new IntervalRange(3,5),root);
        System.out.println("Sum " + sum);

        TraversePrint(root);
    }

    private int query(IntervalRange intervalRange, Node root) {
        if(root.range.isPerfectSubset(intervalRange)){
            sum += root.sum;
            return sum;
         }
        if(root.lChild !=null && root.lChild.range.isPartlySubSet(intervalRange))
             query(intervalRange, root.lChild);
         if(root.rChild !=null && root.rChild.range.isPartlySubSet(intervalRange))
            query(intervalRange, root.rChild);
        return sum;
    }

    private void TraversePrint(Node root) {
        if(root == null) return;

        TraversePrint(root.lChild);
        TraversePrint(root.rChild);
        System.out.println(String.valueOf(root.sum));
    }

    private Node CreateTree(int startIndex, int lastIndex) {
            if (lastIndex == startIndex ) {
               Node leafNode = new Node(array[startIndex]);
                leafNode.lChild = null;
                leafNode.rChild = null;
                leafNode.range = new IntervalRange(startIndex, lastIndex);
                return leafNode;
            }
        int mid = (lastIndex + startIndex) / 2;
        Node leftChild   = CreateTree(startIndex, mid);
        Node rightChild = CreateTree(mid + 1, lastIndex);

        Node newParentNode = new Node(leftChild.sum + rightChild.sum);
        newParentNode.lChild = leftChild;
        newParentNode.rChild = rightChild;
        newParentNode.range = new IntervalRange(leftChild.range.left, rightChild.range.right);
        return  newParentNode;
    }
//
//    private Node findUpdate(int elementIndex, Node root, int valueToUpdate) {
//        if(root == null) return  null;
//
//        if(root.lChild !=null && root.lChild.range.has(elementIndex)) {
//            root.lChild.sum += valueToUpdate;
//            if(root.lChild.range.isPerfectMatch(elementIndex))
//                return root.lChild;
//            else
//                return findUpdate(elementIndex,root.lChild, valueToUpdate);
//        }
//        else {
//            if(root.rChild == null) return null;
//            root.rChild.sum += valueToUpdate;
//                if(root.rChild.range.isPerfectMatch(elementIndex))
//                    return root.rChild;
//                else
//                    return findUpdate(elementIndex,root.rChild,valueToUpdate);
//            }
//    }
    private void findUpdate(int elementIndex, Node root, int valueToUpdate) {
        if(root == null) return;
       root.sum += valueToUpdate;
        if(root.lChild !=null && root.lChild.range.has(elementIndex)) {
                 findUpdate(elementIndex,root.lChild, valueToUpdate);
        }
        else {
            if(root.rChild == null) return;
               findUpdate(elementIndex,root.rChild,valueToUpdate);
            }
    }

    public void lazyUpdate(int elementIndex, Node root, int valueToUpdate){
        if(root == null) return;



    }
    public static void main(String[] args) throws IOException {
        SegmentTree segmentTree = new SegmentTree();
        try {
            segmentTree.create();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    class Node {
        float sum;
        Node lChild;
        Node rChild;
        IntervalRange range;
        public Node(float inputValue) {
            this.sum = inputValue;
        }
    }

    private class IntervalRange {
        IntervalRange(int l, int r){
            left = l;
            right = r;
        }
        int left;
        int right;


        public boolean has(int elementIndex) {
            return  elementIndex >= this.left&& elementIndex <= this.right;
        }

        public boolean isPerfectMatch(int elementIndex) {
            return this.left== elementIndex && this.right == elementIndex;
        }

        public boolean isPerfectSubset(IntervalRange intervalRange) {
            return intervalRange.left <= this.left && intervalRange.right >= this.right;
       }

        public boolean isPartlySubSet(IntervalRange intervalRange) {
            return (this.left <= intervalRange.left && this.right >= intervalRange.left) || (this.left <= intervalRange.right && this.right >=intervalRange.right);
        }
    }

    }
